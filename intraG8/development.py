"""settings of development."""
from intraG8.settings import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'intraG8',
        'USER': 'postgres',
        'PASSWORD': '21338354',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
