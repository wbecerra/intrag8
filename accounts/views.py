
from django.shortcuts import render
from django.contrib.auth import(
    authenticate, login as auth_login,
    logout as auth_logout
)

# from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from .models import User

import json
from django.http import JsonResponse


def login(request):
    mensaje = ''
    print "Entrando a login"
    if request.method == 'POST':
        username = request.POST.get('login')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                return redirect('/')
        mensaje = "Nombre o de usuario o Contrasenia no valido"
        return render(request, 'intra/login.html', {'mensaje': mensaje})
    return render(request, 'intra/login.html', {'mensaje': mensaje})


def logout(request):
    """Sistem logout."""
    auth_logout(request)
    return redirect('/')


def index(request):
    return render(request, 'intra/users.html')


def all_users(request):
    response = {}
    response['users'] = []
    for user in User.objects.all():
        response['users'].append({
            'id': user.id,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'permission': user.function,
        })
    return JsonResponse(response)


def add_user(request):
    print request.POST.get('model')
    data = request.POST.get('model')
    data = json.loads(data)
    print data['first_name']

    user = User(
        username=data['first_name'],
        first_name=data['first_name'],
        last_name=data['last_name'],
        email=data['email'],
        function=data['permission'],
        password=data['password'],
    )
    user.save()
    user.set_password(data['password'])
    user.save()

    return JsonResponse({'status': 'Ok'})


def edit_user(request, user_id):
    print request.POST
    data = request.POST.get('model')
    data = json.loads(data)
    user = User.objects.get(id=user_id)
    print user.id

    if data['password'] != 'no_pass':
        user.set_password(data['password'])
    if data['first_name'] != user.first_name:
        user.username, user.first_name = data['first_name']
    if data['last_name'] != user.last_name:
        user.last_name = data['last_name']
    if data['email'] != user.email:
        user.email = data['email']
    if data['permission'] != user.function:
        user.function = data['permission']
    user.save()

    return JsonResponse({'status': 'Ok'})


def delete_user(request, user_id):
    print "Id a eliminar: ", user_id
    user = User.objects.get(id=user_id)
    user.delete()

    return JsonResponse({'status': 'Ok'})


def get_user_by_id(request, user_id):
    response = {}
    user = User.objects.get(id=user_id)

    response['users'] = []
    response['users'].append({
        'id': user.id,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'email': user.email,
        'permission': user.function,
    })
    return JsonResponse(response)
