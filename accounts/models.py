from django.db import models
from django.contrib.auth.models import AbstractUser

# install by pip install django-fernet-fields


# Create your models here.
class User(AbstractUser):
    """Description: Model Description."""

    function = models.CharField(max_length=150, default='shop')

    class Meta:
        db_table = 'auth_user'
