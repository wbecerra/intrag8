from django.shortcuts import render
from django.contrib.auth.decorators import (
    login_required,
    user_passes_test,
)


# Create your views here.
@login_required(login_url='accounts/login/')
def index(request):
    context = {'welcome': 'Bienvenido'}
    return render(request, 'intra/index.html', context)
