/*jshint multistr: true */
/* intraG8 */

/* Models */



var UsersItem = Backbone.Model.extend({
	defaults: function() {
		return {
			first_name: '',
			last_name: '',
			email: '',
			password: '',
    		permission: '',
		};
	},

	validate: function(attrs){
		message = '';
		expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(attrs.first_name === ''){
			message += 'Es necesario un Nombre de Usuario.\n'
		}
		if(attrs.email === ''){
			message += 'Es necesario el Email del Usuario\n'
		}
		if(attrs.password === ''){
			message += 'Es necesario una Contraseña para el Usuario\n'
		}
		if(! expr.test(attrs.email) && attrs.email != ''){
			message += 'La dirección Email del Usuario es incorrecta.\n';
		}
		if(attrs.permission === 'no_select'){
			message += 'Debe Elegir una Función para el Usuario.\n';
		}
		return message;
	},
});

/* Collections */

var UsersCollection = Backbone.Collection.extend({ 
	model: UsersItem,
    url: function () { 
    	if(this.query)
			return '/accounts/all-users';
		if(this.save)
			return '/accounts/adduser/';
		if(this.query_one != '')
			return '/accounts/get_user_by_id/' + this.query_one + '/';
		if(this.edit != '')
			return '/accounts/edit_user/' + this.edit + '/';
		if(this.delete != '')
			return '/accounts/delete_user/' + this.delete + '/';
	},

    query: false,
    save: false,
    query_one: '',
    edit: '',
    delete: '',

    parse: function(resp) {
		return resp;
	},

});

/* Templates */

var display_all_users = _.template("<tr>\
				<td><%= id %></td>\
				<td><%= first_name %></td>\
				<td><%= last_name %></td>\
				<td><%= email %></td>\
				<td><%= permission %></td>\
				<td style=\"text-align:center;\"><div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"actions-<%= id%>\">\
					<button type=\"button\" class=\"btn btn-primary btn-outline\" \
						onClick=\"user_edit_user_modal(<%= id %>)\">\
							<span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"> Editar</span>\
						</button>\
				</div>\
				<div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"actions-<%= id%>\">\
					<button type=\"button\" class=\"btn btn-danger btn-outline\" \
						onClick=\"delete_user(<%= id %>)\">\
							<span class=\"glyphicon glyphicon-remove-circle\" aria-hidden=\"true\"> Eliminar</span>\
						</button>\
				</div>\
				</td>\
				</tr>");

/* Functions */

/* ---- Users */
var get_all_users = function() {
	var users = new UsersCollection();
	users.query = true;
	users.fetch({success: function (objects, response) {
		$('#user-table-body').empty();
		_.each(response.users, function(user){
			console.log(user);
			$('#user-table-body').append(display_all_users(user));
		});
	}});
};

var user_add_user_modal_save = function(){
	var userItem = new UsersItem();
	var data = {};

	data = {
		'first_name': $('#add_user_first_name').val(),
		'last_name': $('#add_user_last_name').val(),
		'email': $('#add_email').val(),
		'password': $('#add_password').val(),
		'permission': $('#add_permission').val(),
	}

	userItem.set(data);
	if(!userItem.isValid()){
		alert(userItem.validationError);
		
	}else{
		var user = new UsersCollection(userItem);
		user.save = true;
		Backbone.emulateJSON = true;
		user.create(data);
		$('#addUserModal').modal('hide');
		$('#addUserOk').modal('show');
	}

}

var user_edit_user_modal = function(id){
	var user = new UsersCollection();
	user.query_one = id;
	user.fetch({success: function (objects, response) {
		_.each(response.users, function(user){
			$('#edit_user_first_name').val(user.first_name);
			$('#edit_user_last_name').val(user.last_name);
			$('#edit_email').val(user.email);
			$('#edit_permission').val(user.permission);
			$('#edit_save').attr('onClick', 'user_edit_user_modal_save('+ id +')');
			$('#editUserModal').modal('show');

			console.log(user.permission);
		});
	}});
}

var user_edit_user_modal_save = function(id){
	var userItem = new UsersItem();
	var data = {};

	password = $('#edit_password').val();
	if(password === ''){
		password = 'no_pass'
	}

	data = {
		'first_name': $('#edit_user_first_name').val(),
		'last_name': $('#edit_user_last_name').val(),
		'email': $('#edit_email').val(),
		'password': password,
		'permission': $('#edit_permission').val(),
	}

	userItem.set(data);
	if(!userItem.isValid()){
		alert(userItem.validationError);
	}else{
		var user = new UsersCollection(userItem);
		user.edit = id;
		Backbone.emulateJSON = true;
		user.create(data);
		$('#editUserModal').modal('hide');
		$('#addUserOk').modal('show');
	}
}

var delete_user = function(user_id){
	var user = new UsersCollection();
	user.delete = user_id;
	user.fetch({success: function (objects, response) {
		$('#addUserOk').modal('show');
	}});
}